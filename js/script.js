$(document).ready(function(){
	$('.search_container .input_show').click(function(){
		$(this).hide();
		$('.search_container form').show();
		$('.search_container form input').focus();
	})
	$('.search_container form input').focusout(function(){
		$('.search_container .input_show').show();
		$('.search_container form').hide();
	});
});

$(window).on('load', function () {
    var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('.spinner');
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');
});